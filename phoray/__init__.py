from abc import ABCMeta
from enum import Enum
import logging
from typing import NamedTuple
from uuid import uuid4


class IdMixin(metaclass=ABCMeta):

    _id = None

    @property
    def id(self):
        if self._id is None:
            self._id = str(uuid4())
        return self._id


class LoggingMixin(metaclass=ABCMeta):

    _logger = None

    @property
    @classmethod
    def logger(cls):
        if cls._logger is None:
            cls._logger = logging.getLogger(cls.__name__)
        return cls._logger


