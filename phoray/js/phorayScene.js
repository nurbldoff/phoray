function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this,
            args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function phorayScene(sceneData) {
    const loader = new THREE.ObjectLoader();
    console.log(sceneData);
    const scene = loader.parse(sceneData);

    const sceneDomElement = document.getElementById("scene");

    const w = sceneDomElement.clientWidth;
    const h = sceneDomElement.clientHeight;
    const aspect = w / h;
    var camera = new THREE.OrthographicCamera(
        (-1 * aspect) / 2,
        (1 * aspect) / 2,
        1 / 2,
        -1 / 2,
        0.1,
        1000
    );
    var controls = new THREE.OrbitControls(camera, sceneDomElement);
    controls.screenSpacePanning = true;
    controls.enableKeys = false;

    // read back the camera state from the form
    const controlsPositionInput = document.getElementById("controlsPosition");
    const controlsTargetInput = document.getElementById("controlsTarget");
    const controlsZoomInput = document.getElementById("controlsZoom");
    controls.saveState();
    controls.position0.fromArray(JSON.parse(controlsPositionInput.value));
    controls.target0.fromArray(JSON.parse(controlsTargetInput.value));
    controls.zoom0 = parseFloat(controlsZoomInput.value);
    controls.reset();
    /* var geometry = new THREE.BoxGeometry( 1, 1, 1 );
     * var material = new THREE.MeshStandardMaterial( { color: 0x00ff00 } );
     * var cube = new THREE.Mesh( geometry, material );
     * scene.add( cube );
     * console.log(material.toJSON()) */

    var renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(sceneDomElement.clientWidth, sceneDomElement.clientHeight);
    sceneDomElement.appendChild(renderer.domElement);

    // start a render loop
    function animate() {
        requestAnimationFrame(animate);
        controls.update();
        renderer.render(scene, camera);
    }
    // Note: There's no point in re-rendering unless the camera has moved, as of now
    // animate();

    // Add a light that follows the camera
    const light = new THREE.DirectionalLight(0xffffff, 1);
    controls.addEventListener("change", light_update);
    function light_update() {
        light.position.copy(camera.position);
        renderer.render(scene, camera);  // Re-render
    }
    scene.add(light);
    light_update()

    // Make sure camera info is updated in the form
    function updateForm() {
        controls.saveState();
        controlsPositionInput.value = JSON.stringify(controls.position0.toArray());
        controlsTargetInput.value = JSON.stringify(controls.target0.toArray());
        controlsZoomInput.value = controls.zoom0;
    }
    controls.addEventListener("change", debounce(updateForm, 0.5));

    // select element
    sceneDomElement.addEventListener("mousedown", mouseDown, false);
    function mouseDown(event) {
        var rect = event.target.getBoundingClientRect();
        var x0 = event.clientX - rect.left;
        var y0 = event.clientY - rect.top;
        sceneDomElement.addEventListener("mouseup", clickMesh, false);
        function clickMesh(event) {
            sceneDomElement.removeEventListener("mouseup", clickMesh);
            if (event.ctrlKey || event.altKey || event.shiftKey) return; // we're moving camera
            event.preventDefault();
            var mouse = new THREE.Vector2();

            var x = event.clientX - rect.left;
            var y = event.clientY - rect.top;
            if (Math.pow(x - x0, 2) + Math.pow(y - y0, 2) > 10) return; // Not intended as click
            mouse.x = (x / sceneDomElement.clientWidth) * 2 - 1;
            mouse.y = -(y / sceneDomElement.clientHeight) * 2 + 1;
            console.log(mouse.x, mouse.y);
            var raycaster = new THREE.Raycaster();
            raycaster.setFromCamera(mouse, camera);
            var intersects = raycaster.intersectObjects(scene.children, true);
            var mesh;
            for (let i of intersects) {
                if (i.object.type === "Mesh") {
                    mesh = i.object;
                    break;
                }
            }
            if (mesh) {
                // Meshes are grouped and transform is on the group
                controls.target.copy(mesh.parent.position);
            }
        }
    }
}
