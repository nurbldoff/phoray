from inspect import signature

from eliot import start_action, start_task, ActionType


def sanitize(args):
    return {
        arg_name: str(arg_value)
        for arg_name, arg_value in args.items()
    }


def eliot_action():
    def inner(f):
        f_sign = signature(f)
        name = str(f)

        def wrapper(*args, **kwargs):
            bound_args = f_sign.bind(*args, **kwargs)
            sane_args = sanitize(bound_args.arguments)
            with start_action(name=name, **sane_args) as task:
                result = f(*args, **kwargs)
                task.add_success_fields(result=str(result))
            return result

        return wrapper
    return inner


def eliot_task():
    def inner(f):
        f_sign = signature(f)
        name = str(f)

        def wrapper(*args, **kwargs):
            bound_args = f_sign.bind(*args, **kwargs)
            sane_args = sanitize(bound_args.arguments)
            with start_task(name=name, **sane_args) as task:
                result = f(*args, **kwargs)
                task.add_success_fields(result=str(result))
            return result

        return wrapper
    return inner
