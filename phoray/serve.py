import inspect
from itertools import chain, tee
import json
from time import time
from random import seed, randint

from flask import Flask, render_template, send_from_directory, request, url_for, redirect, g
import numpy as np
import pandas as pd
import altair

from phoray.group import Group
from phoray.element import Mirror
from phoray.surface import Sphere
from phoray.source import Source
from phoray.transform import Translation, Rotation


seed(randint(0, 100000))  # keep randomness consistent


app = Flask(__name__, static_url_path='')

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, np.int64):
            return int(obj)
        return json.JSONEncoder.default(self, obj)


app.json_encoder = NumpyEncoder

app.config["TEMPLATES_AUTO_RELOAD"] = True

def webvars(f):

    "Decorate a function to make it return its arguments with the results."

    sig = inspect.signature(f)

    def convert_kwargs(kwargs):
        converted = {}
        for name, para in sig.parameters.items():
            if name in kwargs:
                value = kwargs[name]
                converted[name] = para.annotation(value)
            else:
                converted[name] = para.default
        return converted

    def inner(**kwargs):
        conv = convert_kwargs(kwargs)
        result = f(**conv)
        return result, conv

    return inner


stuff = {}


def serve(f):
    with app.app_context():
        stuff["build"] = webvars(f)


def gen_elements(e):
    if isinstance(e, Group):
        for c in e.children:
            yield from gen_elements(c)
    else:
        yield e


def make_three_geometry_data(surface):
    verts, normals, faces = surface.mesh()
    return {
        "attributes": {
            "position": {
                "itemSize": 3,
                "type": "Float32Array",
                "array": list(chain.from_iterable(verts)),
            },
            "normal": {
                "itemSize": 3,
                "type": "Float32Array",
                "array": list(chain.from_iterable(normals))
            },
        },
        "index": {
            "type": "Uint16Array",
            "array": list(chain.from_iterable(faces)),
        },
        "bones": None,
    }


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def make_three_rays_data(r1, r2):
    return {
        "attributes": {
            "position": {
                "itemSize": 3,
                "type": "Float32Array",
                "array": (
                    list(chain.from_iterable(chain.from_iterable(zip(r1.endpoints, r2.endpoints))))
                    if r2 else
                    list(chain.from_iterable(chain.from_iterable(zip(r1.endpoints, r1.endpoints + r1.directions))))
                )
            }
        }
    }


def build_three_scene(system, rays):
    return {
        "metadata": {
        },
        "geometries": [
            {
                "uuid": elem.surface.id,
                "type": "BufferGeometry",
                "data": make_three_geometry_data(elem.surface),

            }
            for elem in gen_elements(system)
            if getattr(elem, "surface", None)
        ] + [
            {
                "uuid": r1.id,
                "type": "BufferGeometry",
                "data": make_three_rays_data(r1, r2)
            }
            for source_id, source_rays in rays.items()
            for r1, r2 in pairwise(source_rays + [None])
        ],

        "materials": [
	    {
		"uuid": "1004175C-0998-4243-900E-1183CE95A692",
		"type": "MeshStandardMaterial",
		"color": 0x00ff00,
		"roughness": 1,
		"metalness": 0.5,
		"emissive": 0x00ff00,
                "emissiveIntensity": 0.1,
		# "normalScale": [1,1],
		"depthFunc": 3,
		"depthTest": True,
		"depthWrite": True,
		# "skinning": False,
		# "morphTargets": False,
		# "dithering": False,
                "side": 1,  # back
                "metadata": {
                    "version": 4.5,
                    "type": "Material"
                }
	    },
	    {
		"uuid": "A0B70AC4-E92C-4E00-A15A-0AF6DE6C94E8",
		"type": "MeshStandardMaterial",
		"color": 0xff0000,
		"roughness": 1,
		"metalness": 0.5,
		"emissive": 0xff0000,
                "emissiveIntensity": 0.1,
		# "normalScale": [1,1],
		"depthFunc": 3,
		"depthTest": True,
		"depthWrite": True,
		# "skinning": False,
		# "morphTargets": False,
		# "dithering": False,
                "side": 0,  # front
                "metadata": {
                    "version": 4.5,
                    "type": "Material"
                }
	    }, *[
                {
                    "uuid": source.id,
                    "type": "LineBasicMaterial",
                    "color": source.color or 0xffffff,
	            "linewidth": 0.01
                }
                for source in rays
            ]
        ],
        "object": {
            "uuid": system.id,
            "type": "Scene",
            "matrix": system._matglob.flatten(),
            "children": [
                {
                    "uuid": child.id,
                    "name": "xxx",
                    "type": "Group",
                    "matrix": child._matglob.flatten(),
                    "children": [
                        {
                            "uuid": "0a2a6a07-7d86-4dbc-8159-44d3243a427c",
                            "name": "front",
                            "type": "Mesh",
                            "material": "1004175C-0998-4243-900E-1183CE95A692",
                            "geometry": child.surface.id,
                        },
                        {
                            "uuid": "c9b15bdd-77bb-4aa6-8339-f4e2fcd2f85e",
                            "name": "back",
                            "type": "Mesh",
                            "material": "A0B70AC4-E92C-4E00-A15A-0AF6DE6C94E8",
                            "geometry": child.surface.id,
                        }

                    ]
                }
                for child in system.children
                if hasattr(child, "surface")
            ] + [
                {
                    "type": "Group",
                    "name": "rays",
                    "children": [
                        {
                            "uuid": r.id,
                            "type": "LineSegments",
                            "material": source.id,
                            "geometry": r.id,
                        }
                        for source, rays in rays.items()
                        for r in rays
                    ]
                }
            ]
        }
    }


def footprints_to_df(fps):
    return pd.DataFrame({"x": np.concatenate([fp[:, 0] for fp in fps.values()]),
                         "y": np.concatenate([fp[:, 1] for fp in fps.values()]),
                         "wavelength": np.concatenate([fp[:, 2] for fp in fps.values()])})


@app.route("/", methods=["POST"])
def post_index():
    variables = {key: value for key, value in request.form.items()
                 if not key.startswith("_")}
    n_rays = int(request.form.get("n_rays", 100))
    controls_position = json.loads(request.form["_controls_position"])
    controls_target = json.loads(request.form["_controls_target"])
    controls_zoom = float(request.form["_controls_zoom"])

    # Encode all info into the URL and redirect to GET endpoints
    url = url_for("get_index")
    variables_query = "&".join(f"{key}={value}" for key, value in variables.items())
    controls_query = f"_position={controls_position}&_target={controls_target}&_zoom={controls_zoom}"
    return redirect(f"{url}?n_rays={n_rays}&{variables_query}&{controls_query}")


@app.route("/", methods=["GET"])
def get_index():

    controls_position = json.loads(request.args.get("_position", "[0, 0, 50]"))
    controls_target = json.loads(request.args.get("_target", "[0, 0, 0]"))
    controls_zoom = json.loads(request.args.get("_zoom", "1"))

    input_variables = {key: value for key, value in request.args.items()
                       if not key.startswith("_")}
    n_rays = int(request.args.get("n_rays", 100))
    system, variables = stuff["build"](**input_variables);

    t0 = time()
    rays = system.trace(n=n_rays)
    dt = time() - t0

    elements = [(i, el) for i, el in enumerate(gen_elements(system))
                if not isinstance(el, Source)]

    return render_template("serve.html.jinja2",
                           system=build_three_scene(system, rays),
                           variables=variables,
                           controls_position=controls_position,
                           controls_target=controls_target,
                           controls_zoom=controls_zoom,
                           n_rays=n_rays,
                           elements=elements,
                           dt=dt)


@app.route("/footprint/<int:element_i>/", methods=["POST"])
def post_footprint(element_i):
    variables = {key: value for key, value in request.form.items()
                 if not key.startswith("_")}
    n_rays = int(request.form.get("n_rays", 100))
    controls_position = json.loads(request.form.get("_controls_position", "[0, 0, 50]"))
    controls_target = json.loads(request.form.get("_controls_target", "[0, 0, 0]"))
    controls_zoom = float(request.form.get("_controls_zoom", "1"))

    # Encode all info into the URL and redirect to GET endpoints
    url = url_for("get_footprint", element_i=element_i)
    variables_query = "&".join(f"{key}={value}" for key, value in variables.items())
    controls_query = f"_position={controls_position}&_target={controls_target}&_zoom={controls_zoom}"
    return redirect(f"{url}?n_rays={n_rays}&{variables_query}&{controls_query}")


@app.route("/footprint/<int:element_i>/", methods=["GET"])
def get_footprint(element_i):

    controls_position = json.loads(request.args.get("_position", "[0, 0, 50]"))
    controls_target = json.loads(request.args.get("_target", "[0, 0, 0]"))
    controls_zoom = json.loads(request.args.get("_zoom", "1"))

    input_variables = {key: value for key, value in request.args.items()
                       if not key.startswith("_")}
    n_rays = int(request.args.get("n_rays", 100))
    system, variables = stuff["build"](**input_variables);

    t0 = time()
    rays = system.trace(n=n_rays)
    dt = time() - t0

    for i, element in enumerate(gen_elements(system)):
        if i == element_i:
            break

    data = footprints_to_df(element.footprint)

    width = element.surface.xsize
    height = element.surface.ysize

    chart = (altair.Chart(data, width=500, height=500*height/width)
             .mark_circle()
             .encode(
                 x={"field": "x", "type": "quantitative", "scale": {"domain": [-width/2, width/2]}},
                 y={"field": "y", "type": "quantitative", "scale": {"domain": [-height/2, height/2]}},
                 color={
                     "field": "wavelength",
                     "type": "quantitative",
                     "scale": {"range": ["red", "orange", "yellow", "green", "blue"]}
                 }
             )
    )
    print(variables)
    return render_template("footprint.html.jinja2",
                           data=chart.to_json(),
                           variables=variables,
                           n_rays=n_rays,
                           dt=dt)
    return chart.to_json()


@app.route("/footprint/<uuid:element_id>")
def footprint(element_id):
    pass

@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)
