from __future__ import annotations

from collections import defaultdict

from .element import Element
from .transform import Translation, Rotation
from .util import eliot_action


class Group(Element):

    def __init__(self, children:[Element]=None, *args, **kwargs):
        self.children = children or []
        super().__init__(*args, **kwargs)

    def _localize_trace(self, trace):
        return {source: [self.localize(rays[0])]
                for source, rays in trace.items()}

    def trace(self, incoming=None, n=1):
        incoming = incoming or {}
        local_trace = self._localize_trace(incoming)
        outgoing = {}
        for el in self.children:
            local_trace = el.trace(local_trace, n)
            for source, rays in local_trace.items():
                outgoing.setdefault(source, []).extend(self.globalize(r) for r in rays)
        return outgoing


class SequentialItem(Element):

    def __init__(self, element, rotation, distance):
        self.element = element
        self.rotation = rotation
        self.distance = distance

    def __repr__(self):
        return f"<SequentialItem element={self.element}, rotation={self.rotation}, distance={self.distance}"


class SequentialGroup(Group):

    def __init__(self, children:[SequentialElement], args, **kwargs):
        assert "children" not in kwargs, "Children must be added to a SequentialGroup using add()."
        super().__init__(children, *args, **kwargs)

    def add(self, element, rotation=(0, (1, 0, 0)), distance=0):
        seqitem = SequentialItem(element, rotation, distance)
        self.children.append(seqitem)

    def _build_nested_group(self, seqitems):
        seqitem = seqitems[0]
        rotation = Rotation(*seqitem.rotation)
        translation = Translation((0, 0, seqitem.distance))
        if len(seqitems) == 1:
            return Group(children=[seqitem.element], transforms=[rotation, translation])
        return Group(children=[seqitem.element,
                               self._build_nested_group(seqitems[1:])],
                     transforms=[rotation, translation])

    def trace(self, incoming=None, n=1):
        incoming = incoming or {}
        local_trace = self._localize_trace(incoming)
        outgoing = {}
        # calculate as a nested series of groups
        group = self._build_nested_group(self.children)
        return group.trace(incoming, n)
