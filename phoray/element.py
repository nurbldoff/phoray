from __future__ import annotations
from abc import ABCMeta, abstractmethod

from numpy import zeros, ones, dot, array, isfinite, empty

from .ray import Rays
from . import IdMixin, LoggingMixin
from .transformations import (euler_matrix, translation_matrix, concatenate_matrices, inverse_matrix)
from .util import eliot_action


class Element(IdMixin, LoggingMixin, metaclass=ABCMeta):

    """
    Everything in an optical system is an "element".
    This is an abstract baseclass.
    """

    def __init__(self, rotation=None, position=None, transforms:[Transform]=None):
        self.transforms = transforms or []
        if rotation:
            self.transforms.append(Rotation(*rotation))
        if position:
            self.transforms.append(Translate(position))
        self.footprint = {}
        self._calculate_matrices()

    def _calculate_matrices(self):
        self._matglob = concatenate_matrices(*(t.matrix for t in reversed(self.transforms)))
        self._matloc = inverse_matrix(self._matglob)

    def localize_position(self, v):
        """Turn global (relative to the frame) coordinates into local."""
        tmp = ones((len(v), 4))  # make 4-vectors for translations
        tmp[:, :3] = v
        return dot(tmp, self._matloc)[:, :3]

    def localize_direction(self, v):
        """A direction does not change with translation."""
        tmp = zeros((len(v), 4))  # make 4-vectors for translations
        tmp[:, :3] = v
        return dot(tmp, self._matloc)[:, :3]

    def globalize_position(self, v):
        """Turn local coordinates into global."""
        tmp = ones((len(v), 4))  # make 4-vectors for translations
        tmp[:, :3] = v
        return dot(tmp, self._matglob)[:, :3]

    def globalize_direction(self, v):
        """A direction does not change with translation."""
        tmp = zeros((len(v), 4))  # make 4-vectors for translations
        tmp[:, :3] = v
        return dot(tmp, self._matglob)[:, :3]

    def localize(self, rays):
        """
        Transform a Ray in global coordinates into local coordinates
        """
        local_endp = self.localize_position(rays.endpoints)
        local_dir = self.localize_direction(rays.directions)

        return Rays(local_endp, local_dir, rays.wavelengths)

    def globalize(self, rays):
        """
        Transform a local Ray into global coordinates
        """
        global_endp = self.globalize_position(rays.endpoints)
        global_dir = self.globalize_direction(rays.directions)

        return Rays(global_endp, global_dir, rays.wavelengths)

    @eliot_action()
    def trace(self, incoming, n=100):
        "Trace the incoming rays through the element"
        outgoing = {}
        footprint = {}
        for source, rays in incoming.items():
            print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
            new_rays = self.propagate(self.localize(rays[-1]))
            new_rays.origin = self.id
            fp = array((new_rays.endpoints.T[0],
                        new_rays.endpoints.T[1],
                        new_rays.wavelengths))
            # TODO remove rays that missed
            footprint[source] = fp.T[isfinite(fp[0])]
            outgoing[source] = [self.globalize(new_rays)]
        print("footprints", list(footprint.keys()))
        self.footprint = footprint
        return outgoing

    def propagate(self, rays):
        "Return the rays as modified by passing through the element; e.g. reflected."


class Mirror(Element):

    "A mirror reflects incoming rays from its surface."

    def __init__(self, surface:Surface, *args, **kwargs):
        self.surface = surface
        super().__init__(*args, **kwargs)

    @eliot_action()
    def propagate(self, rays):
        return self.surface.reflect(rays)


class Detector(Element):

    """
    A detector does not propagate incoming rays further. It is intended to
    be the final element in a system.
    """

    def __init__(self, surface:Surface, *args, **kwargs):
        self.surface = surface
        super().__init__(*args, **kwargs)

    def propagate(self, rays):
        p = self.surface.intersect(rays)
        nans = zeros((len(rays), 3))
        #nans[:] = (0, 0, 0)
        return Rays(p, nans, rays.wavelengths)


class Screen(Element):

    """
    A screen does not influence the direction of the incoming
    rays, but simply passes them through. Useful for checking the
    intersection of a beam at strategic places.
    """

    def __init__(self, surface:Surface, *args, **kwargs):
        self.surface = surface
        supexbr().__init__(*args, **kwargs)

    def propagate(self, rays):
        p = self.surface.intersect(rays)
        return Rays(p, rays.directions, rays.wavelengths)


class ReflectiveGrating(Element):

    """A reflective grating diffracts incoming rays reflectively."""

    def __init__(self, surface:Surface, d:float=0., order:int=0, *args, **kwargs):
        """
        Define a reflecting element with geometry shape given by s. If
        d>0 it will work as a grating with line spacing d and lines in
        the xz-plane and diffraction order given by order. Otherwise
        it works as a plain mirror.
        """
        self.surface = surface
        self.d = d
        self.order = order
        super().__init__(*args, **kwargs)

    def propagate(self, rays):
        diffracted_rays = self.surface.diffract(
            rays, self.d, self.order)
        return diffracted_rays
