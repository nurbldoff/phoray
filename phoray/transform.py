"""
Various transformations that can be applied on elements.
"""

import abc
from enum import Enum
from math import radians

import numpy as np
from .transformations import (translation_matrix, rotation_matrix,
                              euler_matrix, vector_norm)
from . import IdMixin


class Transform(IdMixin, metaclass=abc.ABCMeta):

    "Abstract baseclass for transformations."


class Translation(Transform):

    def __init__(self, position=(0, 0, 0)):
        self.position = position
        self.matrix = translation_matrix(self.position).T


class Rotation(Transform):

    def __init__(self, angle:float=0, axis=(1, 0, 0)):
        self.angle = angle
        self.axis = axis
        self.matrix = rotation_matrix(radians(self.angle), self.axis)


class Euler(Transform):

    def __init__(self, angles=(0, 0, 0)):
        super().__init__(*args, **kwargs)
        self.angles = angles
        # TODO: make euler order an argument! maybe use enum?
        self.matrix = euler_matrix(*np.radians(self.angles), axes="rxyz")
