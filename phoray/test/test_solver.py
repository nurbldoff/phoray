from math import sqrt
from random import random

from numpy import allclose

from phoray.solver import closest_points


class TestClosestPoint:

    def test_lines_with_same_point(self):
        p1 = (random()-.5, random()-.5, random()-.5)
        r1 = (0, 0, 1)
        r2 = (0, -1, 0)
        s, t = closest_points(p1, r1, p1, r2)
        assert allclose(s, t)

    def test_intersecting_lines(self):
        p1 = (0, 0, 0)
        p2 = (1, 0, 0)
        r1 = (1/sqrt(2), 1/sqrt(2), 0)
        r2 = (0, 1, 0)
        s, t = closest_points(p1, r1, p2, r2)
        assert allclose(s, sqrt(2))
        assert allclose(t, 1)

    def test_skew_lines(self):
        p1 = (0, 0, 0)
        p2 = (1, 0, 0)
        r1 = (1/sqrt(2), 0, 1/sqrt(2))
        r2 = (0, 1, 0)
        s, t = closest_points(p1, r1, p2, r2)
        assert allclose(s, 1/sqrt(2))
        assert allclose(t, 0)
