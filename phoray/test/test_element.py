from random import uniform

from numpy import allclose

from phoray.ray import Rays
from phoray.group import Group
from phoray.transform import Translation, Rotation


A, B, C, D, E, F, G, H, I = [uniform(-0.5, 0.5) for _ in range(9)]


"""TODO: Expand these tests to cover more combinations and edge cases."""

def test_localize_position_translation():
    v1 = [(A, B, C)]
    dv = (D, E, F)
    group = Group(transforms=[Translation(dv)])
    v2 = group.localize_position(v1)
    assert allclose(v1[0], v2[0] + dv)

def test_localize_position_rotation():
    """Euler rotation is applied in the correct order."""
    v1 = [(A, B, C)]
    rot1 = Rotation(90, (1, 0, 0))
    rot2 = Rotation(-90, (0, 0, 1))
    group = Group(transforms=[rot1, rot2])
    v2 = group.localize_position(v1)
    assert allclose((-C, -A, B), v2[0])

def test_localize_position_all():
    """Transformations are applied in the correct order."""
    v1 = [(A, B, C)]
    tr = Translation((D, 0, 0))
    rot = Rotation(-90, (0, 0, 1))
    group = Group(transforms=(tr, rot))
    v2 = group.localize_position(v1)
    assert allclose((B, D-A, C), v2[0])

def test_localize_direction_all():
    """Only rotations are performed on directions."""
    v1 = [(A, B, C)]
    pos = Translation((D, 0, 0))
    rot = Rotation(-90, (0, 0, 1))
    group = Group(transforms=(pos, rot))
    v2 = group.localize_direction(v1)
    assert allclose((B, -A, C), v2[0])

def test_globalize_position_translation():
    v1 = [(A, B, C)]
    dv = Translation((D, E, F))
    group = Group(transforms=[dv])
    v2 = group.globalize_position(v1)
    assert allclose(v1[0], v2[0] - dv.position)

def test_globalize_position_rotation():
    """Rotation is applied in reverse."""
    v1 = [(A, B, C)]
    rot1 = Rotation(90, (1, 0, 0))
    rot2 = Rotation(-90, (0, 0, 1))
    group = Group(transforms=[rot1, rot2])
    v2 = group.globalize_position(v1)
    assert allclose((-B, C, -A), v2[0])

def test_globalize_position_all():
    """Transformations are applied in the correct order."""
    v1 = (A, B, C)
    pos = Translation((D, 0, 0))
    rot = Rotation(-90, (0, 0, 1))
    group = Group(transforms=(pos, rot))
    v2 = group.globalize_position(v1)
    assert allclose((-B+D, A, C), v2[0])

def test_globalizey_direction_all():
    """Transformations are applied in the correct order."""
    v1 = (A, B, C)
    pos = Translation((D, 0, 0))
    rot = Rotation(-90, (0, 0, 1))
    group = Group(transforms=(pos, rot))
    v2 = group.globalize_direction(v1)
    assert allclose((-B, A, C), v2[0])

def test_localize():
    p = [(A, B, C)]
    d = [(D, E, F)]
    pos = Translation((G, H, I))
    r1 = Rays(p, d, None)
    member = Group(transforms=[pos])
    r2 = member.localize(r1)
    assert allclose(r1.endpoints[0], r2.endpoints[0] + pos.position)
    assert allclose(r1.directions[0], r2.directions[0])

def test_globalize():
    p = [(A, B, C)]
    d = [(D, E, F)]
    pos = Translation((G, H, I))
    r1 = Rays(p, d, None)
    member = Group(transforms=[pos])
    r2 = member.globalize(r1)
    assert allclose(r1.endpoints[0], r2.endpoints[0] - pos.position)
    assert allclose(r1.directions[0], r2.directions[0])
