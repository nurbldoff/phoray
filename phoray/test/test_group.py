from math import pi

import eliot
from numpy import allclose

from phoray.group import Group, SequentialGroup
from phoray.element import Mirror
from phoray.surface import Plane
from phoray.transform import Translation, Rotation
from phoray.source import TrivialSource
from phoray.util import eliot_task


eliot.to_file(open("eliot.log", "ab"))


@eliot_task()
def test_group_basic():

    s = TrivialSource()
    p = Plane()
    m = Mirror(surface=p, transforms=[Rotation(45, (0, 1, 0)), Translation((0, 0, 0))])
    g = Group(children=[m], transforms=[Translation((0, 0, 1))])
    system = Group([s, g])
    trace = system.trace(n=1)
    a, b = trace[s.id]
    assert allclose(a.directions, (0, 0, 1))
    assert allclose(b.directions, (1, 0, 0))


def test_sequential_group_basic():

    s = TrivialSource()
    p = Plane()
    m = Mirror(surface=p, transforms=[Rotation(45, (0, 1, 0))])

    sg = SequentialGroup()
    sg.add(s)
    sg.add(m, (0, (1, 0, 0)), 1)
    sg.add(m, (90, (0, 1, 0)), 1)
    trace = sg.trace(n=1)
    x = trace[s.id]
    a, b, c = x
    assert allclose(a.directions, (0, 0, 1))
    assert allclose(b.directions, (1, 0, 0))
    assert allclose(c.directions, (0, 0, -1))
