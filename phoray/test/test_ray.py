from math import sqrt
from random import seed

from numpy import array, allclose

from phoray.ray import Rays
from phoray.surface import Sphere
from phoray.element import Mirror
from phoray.source import GridSource
from phoray.transform import Translation, Rotation


#seed(123)  # make things more predictable


"""
These tests are all pretty crude. The idea is to make qualitative
tests of some simple geometrical setups and test that they behave
as expected. Mainly intended to catch fundamental errors, not
problems with precision.
"""


def test_estimate_focus_two_rays():
    p1 = (0, 0, 0)
    p2 = (1, 0, 0)
    r1 = (1/sqrt(2), 0, 1/sqrt(2))
    r2 = (0, 1, 0)
    rays = Rays([p1, p2], [r1, r2],
                [(0, 0, 0), (0, 0, 0)])
    a = rays.estimate_focus(1)
    assert allclose(a, (0.75, 0.0, 0.25))

def test_estimate_focus_parallel_rays():
    p1 = (0, 0, 0)
    p2 = (1, 0, 0)
    r1 = (0, 1, 0)
    r2 = (0, 1, 0)
    rays = Rays(array((p1, p2)), array((r1, r2)),
                array(((0, 0, 0), (0, 0, 0))))
    a = rays.estimate_focus(1)
    assert a is None

def test_estimate_focus_spherical_mirror():
    sphere = Sphere(1)
    mirror = Mirror(surface=sphere, transforms=[Translation((0, 0, 1))])
    source = GridSource(divergence=(0.1, 0.1, 0))

    rays = {0: [source.generate()]}
    refl = mirror.trace(rays)
    a = refl[0][0].estimate_focus()

    assert allclose(a, (0, 0, 0))

def test_estimate_focus_spherical_mirror_2():
    sphere = Sphere(1)
    mirror = Mirror(surface=sphere,
                    transforms=[Translation((0, 0, 2)),
                                Rotation(30, (1, 0, 0))])
    source = GridSource(divergence=(0.1, 0.1, 0))

    rays = {0: [source.generate()]}
    refl = mirror.trace(rays)
    a = refl[0][0].estimate_focus(100)
    assert allclose(a, (0.0, -0.57, 1.67), atol=0.05)
