from math import cos, radians

from phoray.group import Group
from phoray.element import Mirror, ReflectiveGrating
from phoray.surface import Cylinder
from phoray.source import GaussianSource
from phoray.transform import Translation, Rotation
from phoray.serve import app, serve


@serve
def build_system(in1:float=0.5, in2:float=0.6,
                 angle:float=80,
                 r1:float=1.5, r2:float=1.5, divergence:float=0.01):
    source = GaussianSource(divergence=(divergence, divergence, 0), wavelength=500e-9, color=0xffffff)

    cylinder1 = Cylinder(r1, xsize=0.1, ysize=0.1)
    cylinder2 = Cylinder(r2, xsize=0.1, ysize=0.1)
    deflection = 90 - 2*(90-angle)
    mirror1 = Mirror(cylinder1,
                     transforms=[Translation([0, 0, in1]), Rotation(deflection, [1, 0, 0])])
    mirror2 = Mirror(cylinder2,
                     transforms=[Translation([0, 0, in1]),
                                 Rotation(-2*(90-deflection), [1, 0, 0]),
                                 Translation([0, 0, in2-in1]),
                                 Rotation(90, [0, 0, 1]),
                                 Rotation(-deflection, [1, 0, 0])])

    return Group([source, mirror1, mirror2])


if __name__ == "__main__":
    app.run(port=5002)
