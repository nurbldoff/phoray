from math import sin, cos, acos, asin, radians, degrees

from phoray.group import Group
from phoray.element import Mirror, ReflectiveGrating, Detector
from phoray.surface import Sphere, Plane
from phoray.source import GaussianSource
from phoray.transform import Translation, Rotation
from phoray.serve import app, serve


@serve
def build_system(grating_radius:float=5,
                 grating_density:float=500,
                 incoming_angle:float=5,
                 divergence:float=0.002,
                 #source_distance:float=0.25,
                 grating_order:int=0):

    source_distance = (grating_radius/2) * 2 * sin(radians(incoming_angle))

    source1 = GaussianSource(divergence=(divergence*5, divergence, 0),
                             wavelength=10e-9,
                             color=0xff0000)
    source2 = GaussianSource(divergence=(divergence*5, divergence, 0),
                             wavelength=11e-9,
                             color=0x00ff00)
    source3 = GaussianSource(divergence=(divergence*5, divergence, 0),
                             wavelength=12e-9,
                             color=0x0000ff)


    sphere = Sphere(grating_radius, xsize=0.1, ysize=0.1)
    grating_position = [0, 0, source_distance]
    angle = 90-incoming_angle
    grating_rotation = [-angle, [1, 0, 0]]
    grating = ReflectiveGrating(sphere,
                               transforms=[
                                   Translation(grating_position),
                                   Rotation(*grating_rotation)
                               ],
                               d=0.001/grating_density,
                               order=-grating_order)

    wl = source2.wavelength
    #outgoing_angle = acos(cos(radians(incoming_angle)) - grating_order * wl * grating_density*1000)
    #outgoing_angle = acos(grating_order*wl*grating_density*1000+cos(radians(angle)))
    outgoing_angle = radians(90 + degrees(asin(grating_order*wl*grating_density*1000 - sin(radians(angle)))))
    print("outgoing_angle", degrees(outgoing_angle))
    detector_distance = grating_radius * sin(outgoing_angle)
    detector_position = [0,
                         detector_distance * cos(outgoing_angle),
                         detector_distance * sin(outgoing_angle)]
    plane = Plane(xsize=0.1, ysize=0.1)
    detector = Detector(
        surface=plane,
        transforms=[
            Translation(grating_position),
            Rotation((incoming_angle + degrees(outgoing_angle)), [1, 0, 0]),
            Translation([0, 0, detector_distance]),
            Rotation(-90+degrees(outgoing_angle), [1, 0, 0])
        ]
    )

    return Group([source1, source2, source3, grating, detector])


if __name__ == "__main__":
    app.run(port=5003)
