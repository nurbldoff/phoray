from phoray.group import Group
from phoray.element import Mirror
from phoray.surface import Sphere
from phoray.source import GaussianSource
from phoray.transform import Translation, Rotation
from phoray.serve import app, serve


@serve
def build_system(distance: float = 1,
                 angle: float = 0,
                 divergence: float = 0.02,
                 radius: float = 10):
    source1 = GaussianSource(divergence=(divergence, divergence, 0), wavelength=1e-9)
    source2 = GaussianSource(divergence=(divergence, divergence, 0), wavelength=2e-9)
    sphere = Sphere(radius, xsize=0.1, ysize=0.1)
    mirror = Mirror(sphere,
                    transforms=[Translation([0, 0, distance]),
                                Rotation(angle, [1, 0, 0])])
    return Group([source1, source2, mirror])


if __name__ == "__main__":
    app.run(port=5001)
