import inspect
from itertools import chain, tee
import json
from time import time

from flask import Flask, render_template, send_from_directory, request, url_for, redirect
import numpy as np

from phoray.group import Group
from phoray.element import Mirror
from phoray.surface import Sphere
from phoray.source import GaussianSource
from phoray.transform import Translation, Rotation


app = Flask(__name__, static_url_path='')


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

app.json_encoder = NumpyEncoder

app.config["TEMPLATES_AUTO_RELOAD"] = True

def webvars(f):

    "Decorate a function to make it return its arguments with the results."

    sig = inspect.signature(f)

    def convert_kwargs(kwargs):
        converted = {}
        for name, para in sig.parameters.items():
            if name in kwargs:
                value = kwargs[name]
                converted[name] = para.annotation(value)
            else:
                converted[name] = para.default
        return converted

    def inner(**kwargs):
        conv = convert_kwargs(kwargs)
        result = f(**conv)
        return result, conv

    return inner


@webvars
def build_system(d:float=0.1, angle:float=0, divergence:float=0.01, radius:float=3):
    source = GaussianSource(divergence=(divergence, divergence, 0))
    sphere = Sphere(radius, xsize=0.1, ysize=0.1)
    mirror = Mirror(sphere, [Translation([0, 0, d]), Rotation(angle, [1, 0, 0])])
    return Group([source, mirror])


def gen_elements(e):
    if isinstance(e, Group):
        for c in e.children:
            yield from gen_elements(c)
    else:
        yield e


def make_three_geometry_data(surface):
    verts, normals, faces = surface.mesh()
    return {
        "attributes": {
            "position": {
                "itemSize": 3,
                "type": "Float32Array",
                "array": list(chain.from_iterable(verts)),
            },
            "normal": {
                "itemSize": 3,
                "type": "Float32Array",
                "array": list(chain.from_iterable(normals))
            },
        },
        "index": {
            "type": "Uint16Array",
            "array": list(chain.from_iterable(faces)),
        },
        "bones": None,
    }


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def make_three_rays_data(r1, r2):
    return {
        "attributes": {
            "position": {
                "itemSize": 3,
                "type": "Float32Array",
                "array": (
                    list(chain.from_iterable(chain.from_iterable(zip(r1.endpoints, r2.endpoints))))
                    if r2 else
                    list(chain.from_iterable(chain.from_iterable(zip(r1.endpoints, r1.endpoints + r1.directions))))
                )
            }
        }
    }


def build_three_scene(system, rays):
    return {
        "metadata": {
        },
        "geometries": [
            {
                "uuid": elem.surface.id,
                "type": "BufferGeometry",
                "data": make_three_geometry_data(elem.surface),

            }
            for elem in gen_elements(system)
            if getattr(elem, "surface", None)
        ] + [
            {
                "uuid": r1.id,
                "type": "BufferGeometry",
                "data": make_three_rays_data(r1, r2)
            }
            for r1, r2 in pairwise(rays + [None])
        ],

        "materials": [
	    {
		"uuid": "1004175C-0998-4243-900E-1183CE95A692",
		"type": "MeshStandardMaterial",
		"color": 0xff0000,
		"roughness": 1,
		"metalness": 0.5,
		"emissive": 0xff0000,
                "emissiveIntensity": 0.1,
		# "normalScale": [1,1],
		"depthFunc": 3,
		"depthTest": True,
		"depthWrite": True,
		# "skinning": False,
		# "morphTargets": False,
		# "dithering": False,
                "side": 2,  # double sided,
                "metadata": {
                    "version": 4.5,
                    "type": "Material"
                }
	    },
            {
                "uuid": '971d499c-52f3-40ee-8fb5-01e9a383f016',
                "type": "LineBasicMaterial",
                "color": 0xffffff,
	        "linewidth": 0.01
            }
        ],
        "object": {
            "uuid": system.id,
            "type": "Scene",
            "matrix": system._matglob.flatten(),
            "children": [
                {
                    "uuid": child.id,
                    "name": "xxx",
                    "type": "Mesh",
                    "material": "1004175C-0998-4243-900E-1183CE95A692",
                    "geometry": child.surface.id,
                    "matrix": child._matglob.flatten()
                }
                for child in system.children
                if hasattr(child, "surface")
            ] + [
                {
                    "uuid": r.id,
                    "type": "LineSegments",
                    "material": '971d499c-52f3-40ee-8fb5-01e9a383f016',
                    "geometry": r.id
                }
                for r in rays
            ]
        }
    }


@app.route("/", methods=["POST"])
def post_index():
    variables = {key: value for key, value in request.form.items()
                 if not key.startswith("_")}
    controls_position = json.loads(request.form["_controls_position"])
    controls_target = json.loads(request.form["_controls_target"])
    controls_zoom = float(request.form["_controls_zoom"])

    # Encode all info into the URL and redirect to GET endpoints
    url = url_for("get_index")
    variables_query = "&".join(f"{key}={value}" for key, value in variables.items())
    controls_query = f"_position={controls_position}&_target={controls_target}&_zoom={controls_zoom}"
    return redirect(f"{url}?{variables_query}&{controls_query}")


@app.route("/", methods=["GET"])
def get_index():

    controls_position = json.loads(request.args.get("_position", "[0, 0, 50]"))
    controls_target = json.loads(request.args.get("_target", "[0, 0, 0]"))
    controls_zoom = json.loads(request.args.get("_zoom", "1"))

    input_variables = {key: value for key, value in request.args.items()
                       if not key.startswith("_")}
    system, variables = build_system(**input_variables);

    t0 = time()
    rays = system.trace(n=100)
    dt = time() - t0
    return render_template("webtest_index.jinja2",
                           system=build_three_scene(system, list(rays.values())[0]),
                           variables=variables,
                           controls_position=controls_position,
                           controls_target=controls_target,
                           controls_zoom=controls_zoom,
                           dt=dt)



@app.route("/footprint/<uuid:element_id>")
def footprint(element_id):
    pass

@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)
